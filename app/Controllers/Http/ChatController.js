'use strict'

class ChatController {

    index ({req}) {
        req.socket.emit('chat', 'hello chat controller')
        return "hello"
    }
}

module.exports = ChatController
